<?php 

function phptemplate_form_element($element, $value) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  $output = '<div class="clearfix"';
  if (!empty($element['#id'])) {
    $output .= ' id="' . $element['#id'] . '-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="' . $t('This field is required.') . '">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="' . $element['#id'] . '">' . $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) . "</label>\n";
    }
    else {
      $output .= ' <label>' . $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) . "</label>\n";
    }
  }

  $output .= " $value\n";

  $output .= "</div>\n";

  return $output;
}

function phptemplate_textfield($element) {
  $size = empty($element['#size']) ? '' : ' size="' . $element['#size'] . '"';
  $maxlength = empty($element['#maxlength']) ? '' : ' maxlength="' . $element['#maxlength'] . '"';
  $class = array('form-text xlarge');
  $extra = '';
  $output = '<div class="input">';

  if ($element['#autocomplete_path'] && menu_valid_path(array('link_path' => $element['#autocomplete_path']))) {
    drupal_add_js('misc/autocomplete.js');
    $class[] = 'form-autocomplete';
    $extra =  '<input class="autocomplete" type="hidden" id="' . $element['#id'] . '-autocomplete" value="' . check_url(url($element['#autocomplete_path'], array('absolute' => TRUE))) . '" disabled="disabled" />';
  }
  _form_set_class($element, $class);

  if (isset($element['#field_prefix'])) {
    $output .= '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ';
  }

  $output .= '<input type="text"' . $maxlength . ' name="' . $element['#name'] . '" id="' . $element['#id'] . '"' . $size . ' value="' . check_plain($element['#value']) . '"' . drupal_attributes($element['#attributes']) . ' />';
  if (!empty($element['#description'])) {
    $output .= '<span class="help-block">' . $element['#description'] . "</span>\n";
  }

  if (isset($element['#field_suffix'])) {
    $output .= ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>';
  }
  $output .= '</div>';

  return theme('form_element', $element, $output) . $extra;
}

function phptemplate_textarea($element) {
  $class = array('form-textarea');

  // Add teaser behavior (must come before resizable)
  if (!empty($element['#teaser'])) {
    drupal_add_js('misc/teaser.js');
    // Note: arrays are merged in drupal_get_js().
    drupal_add_js(array('teaserCheckbox' => array($element['#id'] => $element['#teaser_checkbox'])), 'setting');
    drupal_add_js(array('teaser' => array($element['#id'] => $element['#teaser'])), 'setting');
    $class[] = 'teaser';
  }

  // Add resizable behavior
  if ($element['#resizable'] !== FALSE) {
    drupal_add_js('misc/textarea.js');
    $class[] = 'resizable';
  }

  $class[] = 'xxlarge';
  _form_set_class($element, $class);
  $output  = '<div class="input">';
  $output .= '<textarea cols="' . $element['#cols'] . '" rows="' . $element['#rows'] . '" name="' . $element['#name'] . '" id="' . $element['#id'] . '" ' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return theme('form_element', $element, $output);
}

/*function phptemplate_checkboxes($element) {
  return "!";
}*/

function phptemplate_checkbox($element) {
  _form_set_class($element, array('form-checkbox'));
  $checkbox  = '<div class="input">';
  $checkbox .= '<ul class="inputs-list">';
  $checkbox .= '<li>';
  $checkbox .= '<label>';
  $checkbox .= '<input ';
  $checkbox .= 'type="checkbox" ';
  $checkbox .= 'name="' . $element['#name'] . '" ';
  $checkbox .= 'id="' . $element['#id'] . '" ';
  $checkbox .= 'value="' . $element['#return_value'] . '" ';
  $checkbox .= $element['#value'] ? ' checked="checked" ' : ' ';
  $checkbox .= drupal_attributes($element['#attributes']) . ' />' . "\n";
  $checkbox .= '<span>' . $element['#title'] . '</span>';
  $checkbox .= '</label>';
  if (!empty($element['#description'])) {
    $checkbox .= '<span class="help-block">' . $element['#description'] . "</span>\n";
  }
  $checkbox .= '</li>';
  $checkbox .= '</ul>';
  $checkbox .= '</div>';

  unset($element['#title']);
  return theme('form_element', $element, $checkbox);
}

function phptemplate_file($element) {
  _form_set_class($element, array('form-file'));
  $output  = '<div class="input">';
  $output .= '<input type="file" name="'. $element['#name'] .'"'. ($element['#attributes'] ? ' '. drupal_attributes($element['#attributes']) : '') .' id="'. $element['#id'] .'" size="'. $element['#size'] ."\" />\n";
  $output .= '</div>';
  return theme('form_element', $element, $output);
}


function phptemplate_button($element) {
  // Make sure not to overwrite classes.
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'btn form-' . $element['#button_type'] . ' ' . $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'btn form-' . $element['#button_type'];
  }

  return '<input type="submit" ' . (empty($element['#name']) ? '' : 'name="' . $element['#name'] . '" ') . 'id="' . $element['#id'] . '" value="' . check_plain($element['#value']) . '" ' . drupal_attributes($element['#attributes']) . " />\n";
}

function phptemplate_submit($element) {
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = '';
  }
  $element['#attributes']['class'] .= 'primary';
  return theme('button', $element);
}

function phptemplate_select($element) {
  $select = '';
  $size = $element['#size'] ? ' size="' . $element['#size'] . '"' : '';
  _form_set_class($element, array('form-select'));
  $multiple = $element['#multiple'];
  $output  = '<div class="input">';
  $output .= '<select name="' . $element['#name'] . '' . ($multiple ? '[]' : '') . '"' . ($multiple ? ' multiple="multiple" ' : '') . drupal_attributes($element['#attributes']) . ' id="' . $element['#id'] . '" ' . $size . '>' . form_select_options($element) . '</select>';
  $output .= '</div>';
  return theme('form_element', $element, $output);
}

/*
function phptemplate_buttons($element) {
  return "1";
}
*/

function phptemplate_radio($element) {
  _form_set_class($element, array('form-radio'));
  $output  = '<div class="input">';
  $output .= '<ul class="inputs-list">';
  $output .= '<li>';
  $output .= '<label>';
  $output .= '<input type="radio" ';
  $output .= 'id="' . $element['#id'] . '" ';
  $output .= 'name="' . $element['#name'] . '" ';
  $output .= 'value="' . $element['#return_value'] . '" ';
  $output .= (check_plain($element['#value']) == $element['#return_value']) ? ' checked="checked" ' : ' ';
  $output .= drupal_attributes($element['#attributes']) . ' />' . "\n";
  if (!is_null($element['#title'])) {
    $output .= '<span>' . $element['#title'] . '</span>';
  }
  $output .= '</label>';
  $output .= '</li>';
  $output .= '</ul>';
  $output .= '</div>';


  unset($element['#title']);
  return theme('form_element', $element, $output);
}

/*
function phptemplate_radios($element) {
  $class = 'form-radios';
  if (isset($element['#attributes']['class'])) {
    $class .= ' ' . $element['#attributes']['class'];
  }
  $element['#children'] = '<div class="input ' . $class . '">' . (!empty($element['#children']) ? $element['#children'] : '') . '</div>';
  if ($element['#title'] || $element['#description']) {
    unset($element['#id']);
    return theme('form_element', $element, $element['#children']);
  }
  else {
    return $element['#children'];
  }
}*/

function phptemplate_password($element) {
  $size = $element['#size'] ? ' size="' . $element['#size'] . '" ' : '';
  $maxlength = $element['#maxlength'] ? ' maxlength="' . $element['#maxlength'] . '" ' : '';

  _form_set_class($element, array('form-text', 'xlarge'));
  $output  = '<div class="input">';
  $output .= '<input type="password" name="' . $element['#name'] . '" id="' . $element['#id'] . '" ' . $maxlength . $size . drupal_attributes($element['#attributes']) . ' />';
  $output .= '</div>';
  return theme('form_element', $element, $output);
}

function phptemplate_form($element) {
  // Anonymous div to satisfy XHTML compliance.
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'form-stacked ' . $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'form-stacked';
  }
  $action = $element['#action'] ? 'action="' . check_url($element['#action']) . '" ' : '';
  return '<form ' . $action . ' accept-charset="UTF-8" method="' . $element['#method'] . '" id="' . $element['#id'] . '"' . drupal_attributes($element['#attributes']) . ">\n<div>" . $element['#children'] . "\n</div></form>\n";
}

function phptemplate_menu_local_tasks() {
	$output = '';

	if ($primary = menu_primary_local_tasks()) {
		$output .= "<ul class=\"pills\">\n" . $primary . "</ul>\n";
	}
	if ($secondary = menu_secondary_local_tasks()) {
		$output .= "<ul class=\"pills\">\n" . $secondary . "</ul>\n";
	}

	return $output;
}


