<!DOCTYPE html>
<html lang="<?php print $language->language; ?>">
	<head>
		<title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">
    <?php print $scripts; ?>
	</head>
	<body class="<?php print $body_classes; ?>">

  	<div id="common-wrapper">
  		<div class="jumbotron masthead" id="overview">
  			<div class="inner">
  				<div class="container">
  
  					<div id="header" class="row">
  						<div id="logo" class="span8">
  						<?php if ($logo): ?>
  							<a href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /> </a>
              <?php endif; ?>
              <div id="site-name-slogan-wrapper">
	              <div id="site-name"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></div>
              <?php if ($site_slogan): ?>
                <div id="site-slogan"><?php print $site_slogan; ?></div>
              <?php endif; ?>
              </div>
              </div>
  						<div id="header-region" class="span8">
  						  <?php if($header) print $header; ?>
  						</div>
  					</div>

        		<?php if (isset($primary_links) && count($primary_links) > 0) : ?>
        		<div id="primary-menu-wrapper">
              <div class="row"><div class="span16">
                <?php print theme('links', $primary_links, array('class' => 'tabs primary-links')) ?>
              </div></div>
        		</div>
            <?php endif; ?>
  
  				</div> <!-- container -->
  			</div>
  		</div>

      <?php if (isset($secondary_links) && count($secondary_links) > 0) : ?>
  		<div id="secondary-menu-wrapper">
  			<div class="container">
          <div class="row"><div class="span16">
            <?php print theme('links', $secondary_links, array('class' => 'tabs secondary-links')) ?>
          </div></div>
      	</div>
  		</div>
      <?php endif; ?>
  
      <div id="main-content-wrapper">
      	<div class="container">
          <div class="row">
          <?php
          	$left_span_count = 0;
          	$center_span_count = 16;
          	$right_span_count = 0;
          	if ($left) {
          		$left_span_count = 3;
          		$center_span_count -= $left_span_count;
          	}
          	if ($right) {
          		$right_span_count = 3;
          		$center_span_count -= $right_span_count;
          	}
          ?>
          <?php if ($left): ?>
  					<div id="left-sidebar" class="sidebar span<?php print $left_span_count;?>"><?php print $left; ?></div>
  	      <?php endif; ?>
  	      
          	<div class="span<?php print $center_span_count;?>">
  		        <?php if ($breadcrumb) print $breadcrumb; ?>
  		        
  		        <?php if ($title) print '<h1>' . $title . '</h1>'; ?>
  		        <?php if ($messages) print $messages; ?>
  		
  		        <?php if ($is_front && $mission): ?>
  						<div id="mission"><?php print $mission; ?></div>
  		        <?php endif; ?>
  		
  		        <?php if ($help): ?>
  		        <div id="help"><?php print $help; ?></div>
  		        <?php endif; ?>

  		        <?php if ($tabs) print $tabs; ?>
  		        <?php print $content; ?>
  	        </div>
  	        
  	      <?php if ($right): ?>
  					<div id="right-sidebar" class="sidebar span<?php print $right_span_count;?>"><?php print $right; ?></div>
  	      <?php endif; ?>
          </div> <!-- row -->
      	</div> <!-- container -->
      </div> <!-- main-content-wrapper -->
		</div> <!-- common-wrapper -->
  
  	<div id="footer">
  		<div class="container">
  			<div class="row">
  				<div class="span16">
  				  <?php if ($footer_message) print $footer_message; ?>
  				  <?php if ($footer)         print $footer; ?>
  				</div>
  			</div>
  		</div>
  	</div>

  	<?php print $closure; ?>

	</body>
</html>


